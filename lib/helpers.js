import GeoViewport from "@mapbox/geo-viewport";
import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

export const isMarker = (child) =>
  child &&
  child.props &&
  child.props.coordinate &&
  child.props.cluster !== false;

export const calculateBBox = (region) => {
  let lngD;
  if (region.longitudeDelta < 0) lngD = region.longitudeDelta + 360;
  else lngD = region.longitudeDelta;

  return [
    region.longitude - lngD, // westLng - min lng
    region.latitude - region.latitudeDelta, // southLat - min lat
    region.longitude + lngD, // eastLng - max lng
    region.latitude + region.latitudeDelta, // northLat - max lat
  ];
};

export const returnMapZoom = (region, bBox, minZoom) => {
  const viewport =
    region.longitudeDelta >= 40
      ? { zoom: minZoom }
      : GeoViewport.viewport(bBox, [width, height]);

  return viewport.zoom;
};

export const markerToGeoJSONFeature = (marker, index) => {
  return {
    type: "Feature",
    geometry: {
      coordinates: [
        marker.props.coordinate.longitude,
        marker.props.coordinate.latitude,
      ],
      type: "Point",
    },
    properties: {
      point_count: 0,
      index,
      ..._removeChildrenFromProps(marker.props),
    },
  };
};

export const generateSpiral = (marker, clusterChildren, markers, index) => {
  const { properties, geometry } = marker;
  const count = properties.point_count;
  const centerLocation = geometry.coordinates;

  let res = [];
  let angle = 0;
  let start = 0;

  for (let i = 0; i < index; i++) {
    start += markers[i].properties.point_count || 0;
  }

  for (let i = 0; i < count; i++) {
    angle = 0.25 * (i * 0.5);
    let latitude = centerLocation[1] + 0.0002 * angle * Math.cos(angle);
    let longitude = centerLocation[0] + 0.0002 * angle * Math.sin(angle);

    if (clusterChildren[i + start]) {
      res.push({
        index: clusterChildren[i + start].properties.index,
        longitude,
        latitude,
        centerPoint: {
          latitude: centerLocation[1],
          longitude: centerLocation[0],
        },
      });
    }
  }

  return res;
};

export const returnMarkerStyle = (points) => {
  if (points >= 50) {
    return {
      width: 72 / 375 * width,
      height: 72 / 375 * width,
      size: 66 / 375 * width,
      fontSize: 19 / 375 * width,
    };
  }

  if (points >= 25) {
    return {
      width: 66 / 375 * width,
      height: 66 / 375 * width,
      size: 58 / 375 * width,
      fontSize: 18 / 375 * width,
    };
  }

  if (points >= 15) {
    return {
      width: 60 / 375 * width,
      height: 60 / 375 * width,
      size: 52 / 375 * width,
      fontSize: 17 / 375 * width,
    };
  }

  if (points >= 10) {
    return {
      width: 54 / 375 * width,
      height: 54 / 375 * width,
      size: 46 / 375 * width,
      fontSize: 16 / 375 * width,
    };
  }

  if (points >= 8) {
    return {
      width: 48 / 375 * width,
      height: 48 / 375 * width,
      size: 40 / 375 * width,
      fontSize: 16 / 375 * width,
    };
  }

  if (points >= 4) {
    return {
      width: 42 / 375 * width,
      height: 42 / 375 * width,
      size: 36 / 375 * width,
      fontSize: 15 / 375 * width,
    };
  }

  return {
    width: 36 / 375 * width,
    height: 36 / 375 * width,
    size: 30 / 375 * width,
    fontSize: 14 / 375 * width,
  };
};

const _removeChildrenFromProps = (props) => {
  const newProps = {};
  Object.keys(props).forEach((key) => {
    if (key !== "children") {
      newProps[key] = props[key];
    }
  });
  return newProps;
};
